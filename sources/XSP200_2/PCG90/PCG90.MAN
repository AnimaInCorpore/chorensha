■□■□■□■□■□■□■□■□■□■□■□■□■□■□■□■□■□■□■
□　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　□
■　　　　　　　　　 ＸＳＰ支援　ＰＣＧ９０度回転関数 　　　　　　　　　■
□　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　□
■　 　　　　　　　　 Ｃ言語＆アセンブラ用ライブラリ　　　　　　　　　　■
□　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　□
■　　　　　　ＶＥＲ．１．０２　　　　　Ｂｙ　ファミベのよっしん　　　　■
□　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　□
■□■□■□■□■□■□■□■□■□■□■□■□■□■□■□■□■□■□■




□■□■□■□■□■□■□■□■□■□■□■□■□■□■□■□■□■□■□

　　　　　　　　　　　　　本ライブラリの説明

□■□■□■□■□■□■□■□■□■□■□■□■□■□■□■□■□■□■□


　本ライブラリを用いることにより、１６×１６ドット及び８×８ドットのＰＣＧ

データを、右又は左に９０度回転したように書き換えることができます。本ライブ

ラリが対応しているのは、Ｘ６８ＫのＰＣＧエリアと同じベタ形式フォーマットの

ＰＣＧデータです（ＸＳＰシステムで使うフォーマットと同じです）。

　ＸＳＰシステムの縦画面モードを使用する場合の、ＰＣＧデータの書き換え等に

お勧めです。もちろん、ＸＳＰシステム以外の用途で用いることもできます。




□■□■□■□■□■□■□■□■□■□■□■□■□■□■□■□■□■□■□

　　　　　　　　　　　　　　　各関数の説明

□■□■□■□■□■□■□■□■□■□■□■□■□■□■□■□■□■□■□


　以下に各関数の使用法を説明します。


--------------------------------------------------------------------------

●　ｐｃｇ＿ｒｏｌｌ９０

　書式：void  pcg_roll90( void *pcg , int lr ) ;

　引数：void *pcg : １６×１６ドットのＰＣＧデータへのポインター

　　　　int  lr   :　１のとき右９０度回転
　　　　　　　　　 −１のとき左９０度回転

　戻り値：無し


　★アセンブラ対応版について

　　　ＩＮ　　　　：Ａ０．Ｌ = *pcg
　　　　　　　　　　Ｄ０．Ｌ = lr

　　　ＯＵＴ　　　：無し

　　　破壊レジスタ：無し


　機能：指定されたアドレスから始まる１６×１６ドットのＰＣＧデータ１個（＝

　　　１２８バイト）を、指定方向に９０度回転させます。



--------------------------------------------------------------------------

●　ｂｇｐｃｇ＿ｒｏｌｌ９０

　書式：void  bgpcg_roll90( void *pcg , int lr ) ;

　引数：void *pcg : ８×８ドットのＰＣＧデータへのポインター

　　　　int   lr  :　１のとき右９０度回転
　　　　　　　　　 −１のとき左９０度回転

　戻り値：無し


　★アセンブラ対応版について

　　　ＩＮ　　　　：Ａ０．Ｌ = *pcg
　　　　　　　　　　Ｄ０．Ｌ = lr

　　　ＯＵＴ　　　：無し

　　　破壊レジスタ：無し


　機能：指定されたアドレスから始まる８×８ドットのＰＣＧデータ１個（＝３２

　　　バイト）を、指定方向に９０度回転させます。



□■□■□■□■□■□■□■□■□■□■□■□■□■□■□■□■□■□■□

　　　　　　　　　　　　　バージョンＵＰ履歴


　　　◎：バグ及び不具合いの修正に関するもの。

　　　●：改良点及び変更点その他に関するもの。

□■□■□■□■□■□■□■□■□■□■□■□■□■□■□■□■□■□■□


ＶＥＲ．１．００：（１９９４／８）

　　●　とりあえず完成版。



ＶＥＲ．１．０１：（１９９５／１１）

　　◎　pcg_roll関数及び、bgpcg_roll関数のアセンブラ版をグローバル宣言し忘

　　　れていたので修正。


ＶＥＲ．１．０２：（１９９６／８）

　　●　マニュアルの書式を若干修正。

　　●　ヘッダを作成。



