
;	xdef L_00C00000,L_00E00000,L_00E20000,L_00E40000,L_00E60000,L_00E80000
;	xdef L_00E82000,L_00E84000,L_00E86000,L_00E88000,L_00E8A000,L_00E8C000
;	xdef L_00E8E000,L_00E90000,L_00E92000,L_00E94000,L_00E96000,L_00E96021
;	xdef L_00E98000,L_00E9A000,L_00E9E000,L_00EA0000,L_00EAF900,L_00EAFA00
;	xdef L_00EAFA10,L_00EAFB00,L_00EB0000,L_00EB8000,L_00EC0000,L_00ED0000
;	xdef L_00ED4000,L_00EF0000,L_00F00000,L_00FC0000,L_00FE0000

	xdef L_00E80000,L_00E82000,L_00E88000,L_00E8E000,L_00EB0000,L_00EB8000,L_00E00000,L_00E20000

	xdef L_00000118,L_00000138

; ------------------------------------------------------------------------------
	text
; ------------------------------------------------------------------------------

dummy_interrupt_handler:
	rte

; ------------------------------------------------------------------------------
	data
; ------------------------------------------------------------------------------

	even

L_00000118:
	dc.l	dummy_interrupt_handler

L_00000138:
	dc.l	dummy_interrupt_handler

; ------------------------------------------------------------------------------
	bss
; ------------------------------------------------------------------------------

	align 4

;L_00C00000:   ; VRAM
;	ds.b    $200000
L_00E00000:   ; TEXT PLANE 1
	ds.b    $20000
L_00E20000:   ; TEXT PLANE 2
	ds.b    $20000
L_00E40000:   ; TEXT PLANE 3
	ds.b    $20000
L_00E60000:   ; TEXT PLANE 4
	ds.b    $20000
L_00E80000:   ; CRTC
	ds.b    $2000
L_00E82000:   ; VIDEO CONTROLLER
	ds.b    $2000
;L_00E84000:   ; DMAC
;	ds.b    $2000
;L_00E86000:   ; AREA
;	ds.b    $2000
L_00E88000:   ; MFP
	ds.b    $2000
;L_00E8A000:   ; RTC
;	ds.b    $2000
;L_00E8C000:   ; PRINTER
;	ds.b    $2000
L_00E8E000:   ; SYSTEM PORT
	ds.b    $2000
;L_00E90000:   ; FM
;	ds.b    $2000
;L_00E92000:   ; ADPCM
;	ds.b    $2000
;L_00E94000:   ; FDC
;	ds.b    $2000
;L_00E96000:   ; HDD
;	ds.b    $21
;L_00E96021:   ; SCSI (HDD)
;	ds.b    $1FDF
;L_00E98000:   ; SCC
;	ds.b    $2000
;L_00E9A000:   ; I/O
;	ds.b    $4000
;L_00E9E000:   ; FPU
;	ds.b    $2000
;L_00EA0000:   ; SCSI
;	ds.b    $F900
;L_00EAF900:   ; FAX
;	ds.b    $100
;L_00EAFA00:   ; MIDI 1
;	ds.b    $10
;L_00EAFA10:   ; MIDI 2
;	ds.b    $F0
;L_00EAFB00:   ; (ToDo)
;	ds.b    $500
L_00EB0000:   ; SPRITE REGISTERS
	ds.b    $8000
L_00EB8000:   ; SPRITE VRAM
	ds.b    $8000
;L_00EC0000:   ; USER I/O
;	ds.b    $10000
;L_00ED0000:   ; SRAM
;	ds.b    $4000
;L_00ED4000:   ; 'PRELIMINARY'
;	ds.b    $1C000
;L_00EF0000:   ; 'UNUSED'
;	ds.b    $10000
;L_00F00000:   ; CG ROM
;	ds.b    $C0000
;L_00FC0000:   ; 'PRELIMINARY'
;	ds.b    $20000
;L_00FE0000:   ; IPL ROM
;	ds.b    $1FFFF

; ------------------------------------------------------------------------------
	end
; ------------------------------------------------------------------------------

